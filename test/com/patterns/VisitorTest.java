package com.patterns;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.patterns.visitor.Book;
import com.patterns.visitor.PostageVisitor;
import com.patterns.visitor.Visitable;

public class VisitorTest {
		
	@Test
	public void test1 (){
		List<Visitable> items = new ArrayList<>();
		items.add(new Book(10, 3.4));
		items.add(new Book(7,  7.4));
		items.add(new Book(8,  8.4));
		items.add(new Book(11, 3.4));
		items.add(new Book(22, 4.4));
		
		//create a visitor
	    PostageVisitor visitor = new PostageVisitor();
	    //iterate through all items
	    for(Visitable item: items) {
	      item.accept(visitor);
	    }
	    double postage = visitor.getTotalPostage();
		
		System.out.println("Total postage is: " + postage);
		Assert.assertTrue(postage == 31.6);
	}
	
}
