package com.patterns.singleton;

import org.junit.Test;

public class SingleObjectTest {

	@Test
	public void test() {
		SingleObject object = SingleObject.getInstance();
		object.showMessage();
	}

}
