package com.patterns.prototype;

import static org.junit.Assert.assertThat;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

public class PrototypeTest {
   
	@Test
	public void test() {
		ShapeCache.loadCache();

		Shape clonedShape1 = (Shape) ShapeCache.getShape("1");
		System.out.println("Shape 1 : " + clonedShape1.getType());
		assertThat(clonedShape1.getType(), is("Circle"));

		Shape clonedShape2 = (Shape) ShapeCache.getShape("2");
		System.out.println("Shape 2 : " + clonedShape2.getType());
		assertThat(clonedShape2.getType(), is("Square"));

		Shape clonedShape3 = (Shape) ShapeCache.getShape("3");
		System.out.println("Shape 3 : " + clonedShape3.getType());
		assertThat(clonedShape3.getType(), is("Rectangle"));
	}

}
