package com.patterns.factory;

import static org.hamcrest.CoreMatchers.startsWith;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FactoryTest {

	private final PrintStream stdOutStream = System.out;
	private final PrintStream stdErrStream = System.err;

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	ShapeFactory shapeFactory = new ShapeFactory();

	@Before
	public void setUpStreams() {
		outContent.reset();
		errContent.reset();
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}

	@Test
	public void test1() {
		// get an object of Circle and call its draw method.
		Shape shape1 = shapeFactory.getShape("CIRCLE");
		shape1.draw();
		// check the printed stream
		Assert.assertThat(outContent.toString(), startsWith("Inside Circle::draw() method."));
	}

	@Test
	public void test2() {
		// get an object of Rectangle and call its draw method.
		Shape shape2 = shapeFactory.getShape("RECTANGLE");
		shape2.draw();
		Assert.assertThat(outContent.toString(), startsWith("Inside Rectangle::draw() method."));
	}

	@Test
	public void test3() {

		// get an object of Square and call its draw method.
		Shape shape3 = shapeFactory.getShape("SQUARE");
		shape3.draw();
		Assert.assertThat(outContent.toString(), startsWith("Inside Square::draw() method."));
	}

	@After
	public void restoreStreams() {
		System.setOut(stdOutStream);
		System.setErr(stdErrStream);

		System.out.println("After : " + outContent.toString());
	}
}
