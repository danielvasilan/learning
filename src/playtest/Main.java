package playtest;

public class Main {

	public static void main(String[] args) {
		TestStreams test = new TestStreams();
		test.init();
		
		//test.getData().stream().forEach(System.out::println);
		test.getGrouped().entrySet().stream().forEach(System.out::println);
		//test.getGrouped2x().entrySet().stream().forEach(System.out::println);
		
	}

}
