package playtest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestStreams {
	List<MyReportData> reports = new ArrayList<>();
	
	public void init(){
	    for (int i = 1; i < 100; i++) {
	    	reports.add(new MyReportData("report_" + i, "c"+i%3, "r"+i%5, 101.01));
	    }
	}
	
	public List<MyReportData> getData (){
		return reports;
	}
	
	
	public Map<String, Double> getGrouped(){
		//return reports.stream().collect(Collectors.groupingBy(Collectors.summingDouble(groupBy)));
		return reports.stream()
				.collect(Collectors.groupingBy(MyReportData::getColName, 
						Collectors.summingDouble(MyReportData::getAmount)));
	}

/*	public Map<?, Double> getGrouped2x(){
		//return reports.stream().collect(Collectors.groupingBy(Collectors.summingDouble(groupBy)));
		return reports.stream()
				.collect(Collectors.groupingBy((a) -> {Set v = new HashSet(); v.add(a.getColName()); v.add(a.getRowName()); return v; }, 
						Collectors.summingDouble(MyReportData::getAmount), Collectors.summingDouble(MyReportData::getAmount)));
	}	
*/	
	/*
	private Function<MyReportData, ?> groupBy = (reportDataCell) -> {
		/*Map<String, Double> filterFields = reportDataCell.getFields().entrySet().stream()
				.filter(map -> groupByFields.contains(map.getKey()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		ReportDataCell newReportDataCell = new ReportDataCell();
		newReportDataCell.addFields(filterFields);
		return newReportDataCell;
		return reports.stream().collect(groupingBy(MyReportData::colname), Collectors.toList());
		//, Collectors.counting());
	};
	*/
}
