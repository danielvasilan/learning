package playtest;

import java.io.Serializable;



public class MyReportData implements Serializable{

    private static final long serialVersionUID = 1L;
    String reportName;
    String colName;
    String rowName;
    Double amount;
    
    public MyReportData(String reportName, String colName, String rowName, Double amount) {
        super();
        this.reportName = reportName;
        this.colName = colName;
        this.rowName = rowName;
        this.amount = amount;
    }

    public String getReportName() {
        return reportName;
    }
    
    public String toString(){
    	return "Report: "+this.reportName+"; col: "+this.colName+"; row: "+this.rowName+"; am: "+this.amount;
    }

	public String getColName() {
		return colName;
	}

	public String getRowName() {
		return rowName;
	}

	public Double getAmount() {
		return amount;
	}
    
}