package reportdata;

import java.util.ArrayList;
import java.util.List;

public class ReportRow {
	private final String rowName;
	private ArrayList<String> dynamicFields = new ArrayList<>();
	
	public ReportRow (String rowName){
		this.rowName = rowName;
	}

	public List<String> getDynamicFields() {
		return dynamicFields;
	}

	public void setDynamicFields(ArrayList<String> dynamicFields) {
		this.dynamicFields = dynamicFields;
	}

	public String getRowName() {
		return rowName;
	}
	 
}
