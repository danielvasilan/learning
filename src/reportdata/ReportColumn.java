package reportdata;

import java.util.ArrayList;
import java.util.List;

public class ReportColumn {
	private final String columnName;
	private ArrayList<String> dynamicFields = new ArrayList<>();
	
	public ReportColumn (String columnName){
		this.columnName = columnName;
	}

	public List<String> getDynamicFields() {
		return dynamicFields;
	}

	public void setDynamicFields(ArrayList<String> dynamicFields) {
		this.dynamicFields = dynamicFields;
	}

	public String getColumnName() {
		return columnName;
	}
}
