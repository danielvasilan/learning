package reportdata;

import java.util.Collections;

public class ReportTablePrinter {
	
	private static Integer ColumnLength = 20;
	
	static public void printReport(ReportTable report){
		for (ReportPage page : report.getReportStructure().pages){
			printPage(report, page);
		}
	}
	
	static void printHeader (ReportTable report){
		System.out.printf("%"+ColumnLength+"s", "|");
		for (ReportColumn column : report.getReportStructure().columns){
			if (!report.colDistKeys.containsKey(column.getColumnName())){
				System.out.printf("%" + ColumnLength + "s", column.getColumnName());
			} else {
				for (String distKey : report.colDistKeys.get(column.getColumnName())){
					String distKeyPrint = distKey.equals("#") ? "" : " > " + distKey;
					System.out.printf("%"+ColumnLength+"s", column.getColumnName() + distKeyPrint);				
				}
			}
		}
		System.out.println("\n"+String.join("", 
				Collections.nCopies(ColumnLength*(report.getReportStructure().columns.size()+1), "-")));
	} 

	static void printPage(ReportTable report, ReportPage page){
		System.out.println("\nPage " + page.getPageName());
		printHeader(report);
		for (ReportRow row : report.getReportStructure().rows){
			printRow(report, row);
		}
	}
	
	static void printRow (ReportTable report, ReportRow row){
		if (report.rowDistKeys.containsKey(row.getRowName())){
			for (String rowKey : report.rowDistKeys.get(row.getRowName())){
				printSingleRow (report, row, rowKey);
			}
		} else {
			printSingleRow (report, row, "");
		}
	}
	
	static void printSingleRow (ReportTable report, ReportRow row, String rowKey){
		// get data for the current position
		String distKeyPrint = rowKey.equals("#") ? "" : " > " + rowKey;
		System.out.printf("\n%-"+ColumnLength+"s", row.getRowName() + distKeyPrint);
		for (ReportColumn column : report.getReportStructure().columns){
			ReportDataPosition data = report.getDataPosition(row.getRowName()+";"+column.getColumnName());
			if (data != null && !data.getRecords().isEmpty()){
				if (report.colDistKeys.containsKey(column.getColumnName())){
					for (String distColKey : report.colDistKeys.get(column.getColumnName())){
						System.out.printf("%"+ColumnLength+"s", data.getAmount (rowKey, distColKey));
					}
				} else {
					System.out.printf("%"+ColumnLength+"s", data.getAmount (rowKey, "#"));
				}
			} else {
				System.out.printf("%"+ColumnLength+"s", " ");
			}
		}
		
		


	}
	
	
}