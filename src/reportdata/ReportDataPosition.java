package reportdata;

import java.util.HashMap;

public class ReportDataPosition {
	private HashMap<String, HashMap<String, ReportDataRecord>> records = new HashMap<>();
	private final String positionName;
	private final String rowName;
	private final String columnName;
	
	public void addRecord (ReportDataRecord newRecord, ReportTable report){
		ReportRow rowConfig = report.getReportStructure().getRowConfig(rowName);
		ReportColumn colConfig = report.getReportStructure().getColumnConfig(columnName);
		String dynRowKey = "";
		if (rowConfig.getDynamicFields().isEmpty()){
			dynRowKey = "#";
		} else {
			for ( String fieldName : rowConfig.getDynamicFields()){
				dynRowKey = dynRowKey + newRecord.getFieldValues().get(fieldName)+ "#";
			}
		}
		String dynColKey = "";
		if (colConfig.getDynamicFields().isEmpty()) {
			dynColKey = "#";
		} else {
			for ( String fieldName : colConfig.getDynamicFields()){
				dynColKey = dynColKey + newRecord.getFieldValues().get(fieldName)+ "#";
			}
		}
		
		HashMap<String, ReportDataRecord> dynColRecords; 
		if (records.containsKey(dynRowKey)){
			dynColRecords = records.get(dynRowKey);
		} else {
			dynColRecords = new HashMap<String, ReportDataRecord>();
			records.put(dynRowKey, dynColRecords);
		}
		ReportDataRecord record;
		if (dynColRecords.containsKey(dynColKey)){
			record = dynColRecords.get(dynColKey);
			record.setAmount(record.getAmount() + newRecord.getAmount());
		} else {
			dynColRecords.put(dynColKey, newRecord);
		}
		
		// 
		report.addRowDistKey(rowName, dynRowKey);
		report.addColDistKey(columnName, dynColKey);
		
	}
	
	public Double getAmount (String rowKey, String columnKey){
		if (records.containsKey(rowKey)){
			if (records.get(rowKey).containsKey(columnKey)){
				return records.get(rowKey).get(columnKey).getAmount();
			}
		}
		return null;
	}
	
	public HashMap<String, HashMap<String, ReportDataRecord>> getRecords(){
		return records;
	}
	
	public ReportDataPosition(String positionName, String rowName, String columnName){
		this.positionName = positionName;
		this.rowName = rowName;
		this.columnName = columnName;
	}

	public String getPositionName() {
		return positionName;
	}
}
