package reportdata;

import java.util.ArrayList;
import java.util.List;

public class ReportPage {
	private final String pageName;
	private ArrayList<String> dynamicFields = new ArrayList<>();
	
	public ReportPage(String pageName){
		this.pageName = pageName;
	}
	public String getPageName() {
		return pageName;
	}
	public List<String> getDynamicFields() {
		return dynamicFields;
	}

	public void setDynamicFields(ArrayList<String> dynamicFields) {
		this.dynamicFields = dynamicFields;
	}
}
