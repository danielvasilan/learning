package reportdata;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ReportTable {
	private ReportStructure reportStructure = new ReportStructure();
	private Map<String, ReportDataPosition> data = new HashMap<>();
	public Map<String, Set<String>> rowDistKeys = new HashMap<>();
	public Map<String, Set<String>> colDistKeys = new HashMap<>();
	
	
	public void loadRecord (String rowName, String columnName, ReportDataRecord record){
		String positionName = rowName + ";" + columnName;
		if (data.containsKey(positionName)){
			data.get(positionName).addRecord(record, this);
		} else {
			ReportDataPosition position = new ReportDataPosition(positionName, rowName, columnName);
			position.addRecord(record, this);
			data.put(positionName, position);
		}
	}
	
	public ReportDataPosition getDataPosition(String positionName){
		if (data.containsKey(positionName)){
			return data.get(positionName);
		} else {
			return null;
		}
	}
	
	public void addRowDistKey(String rowName, String rowKey){
		if (this.rowDistKeys.containsKey(rowName)){
			this.rowDistKeys.get(rowName).add(rowKey);
		} else {
			HashSet<String> distKeys = new HashSet<>();
			distKeys.add(rowKey);
			this.rowDistKeys.put(rowName, distKeys);
		}
	}
	
	public void addColDistKey(String colName, String colKey){
		if (this.colDistKeys.containsKey(colName)){
			this.colDistKeys.get(colName).add(colKey);
		} else {
			HashSet<String> distKeys = new HashSet<>();
			distKeys.add(colKey);
			this.colDistKeys.put(colName, distKeys);
		}
	}
	
	public ReportPage addPage(String pageName){
		ReportPage page = new ReportPage(pageName);
		reportStructure.addPage(pageName);
		return page;
	}

	public void setReportStructure(ReportStructure reportStructure) {
		this.reportStructure = reportStructure;
	}

	public ReportStructure getReportStructure() {
		return reportStructure;
	}
	
}

