package reportdata;

import java.util.HashMap;
import java.util.Map;

public class ReportDataRecord {
	private Map<String, String> fieldValues = new HashMap<>();
	private Double amount;
	
	public ReportDataRecord(Map<String, String> fieldValues, Double value) {
		super();
		this.fieldValues = fieldValues;
		this.amount = value;
	}
	
	public Double getAmount (){
		return this.amount;
	}
	public void setAmount(Double amount){
		this.amount = amount;
	}
	public Map<String, String> getFieldValues(){
		return this.fieldValues;
	}
	
}
