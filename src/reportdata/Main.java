package reportdata;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

	public static void main(String[] args) {
		ReportTable report = new ReportTable();

		ReportStructure structure = new ReportStructure();
		
		structure.addColumn("C1");
		structure.addColumn("C2");
		ArrayList<String> dynFields1 = new ArrayList<String>();
		dynFields1.add("C007");
		structure.addColumn("C3").setDynamicFields(dynFields1);;
		structure.addColumn("C4");
		
		structure.addRow("R1");
		structure.addRow("R2");
		ArrayList<String> dynFields = new ArrayList<String>();
		dynFields.add("C010");
		structure.addRow("R3").setDynamicFields(dynFields);
		structure.addRow("R4");
		
		report.setReportStructure(structure);
		
		//report.loadData();
		/* load dummy data */
		report.loadRecord("R1","C1", dummyData(10.00, "RON", "RO", ""));
		report.loadRecord("R1", "C3", dummyData(11.00, "EUR", "RO", ""));
		report.loadRecord("R1", "C3", dummyData(17.00, "RON", "RO", ""));
		report.loadRecord("R3", "C3", dummyData(19.00, "EUR", "RO", ""));
		report.loadRecord("R3", "C3", dummyData(23.00, "RON", "RO", ""));
		report.loadRecord("R3", "C3", dummyData(39.00, "EUR", "IT", ""));
		report.loadRecord("R3", "C3", dummyData(59.00, "EUR", "DE", ""));
		report.loadRecord("R3", "C3", dummyData(63.00, "RON", "DE", ""));
		
		report.addPage("RO");
		report.addPage("DE");
		
		ReportTablePrinter.printReport(report);
	}

	static ReportDataRecord dummyData(double amount, String c007, String c010, String bla){
		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put("C007", c007);
		fields.put("C010", c010);
		fields.put("BLA", bla);
		ReportDataRecord rec = new ReportDataRecord(fields, amount);
		
		return rec;
		
	}
	
}
