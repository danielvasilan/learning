package reportdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReportStructure{
	List<ReportPage> pages = new ArrayList<>();
	List<ReportRow> rows = new ArrayList<>();
	List<ReportColumn> columns = new ArrayList<>();
	
	HashMap<String, ReportPage> idxPages = new HashMap<>();
	HashMap<String, ReportRow> idxRows = new HashMap<>();
	HashMap<String, ReportColumn> idxColumns = new HashMap<>();

	public ReportPage addPage(String pageName){
		ReportPage page = new ReportPage(pageName);
		this.pages.add(page);
		this.idxPages.put(pageName, page);
		return page;
	}
	
	public ReportRow addRow(String rowName){
		ReportRow row = new ReportRow(rowName);
		this.rows.add(row);
		this.idxRows.put(rowName, row);
		return row;
	}
	
	public ReportColumn addColumn(String columnName){
		ReportColumn column = new ReportColumn(columnName);
		this.columns.add(column);
		this.idxColumns.put(columnName, column);
		return column;
	}
	
	public ReportRow getRowConfig(String rowName){
		return this.idxRows.get(rowName);
	}
	public ReportColumn getColumnConfig(String columnName){
		return this.idxColumns.get(columnName);
	}
	
}