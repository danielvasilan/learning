package com.concurrency;

import org.fusesource.jansi.AnsiConsole;

import static org.fusesource.jansi.Ansi.Color.*;

/* java -cp ./bin;./lib/jansi-1.16.jar com.concurrency.Challenge1 */

public class Challenge1 {

	public static void main(String[] args) {
	    AnsiConsole.systemInstall();
		
		BankAccount account = new BankAccount("12345-678", 1000.00);
		account.printBalance(GREEN);

		new Thread (){
			public void run(){
					account.deposit(300.00, RED);
					account.printBalance(RED);
					account.withdraw(50.00, RED);
					account.printBalance(RED);
				
			} 
		}.start();
		
		new Thread (){
			public void run(){
					account.deposit(203.75, BLUE);
					account.printBalance(BLUE);
					account.withdraw(100.00, BLUE);
					account.printBalance(BLUE);
			}
		}.start();
		
		synchronized(account){
			account.printBalance(GREEN);
		}
	}

}
