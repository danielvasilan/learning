package com.concurrency;

import org.fusesource.jansi.AnsiConsole;
import static org.fusesource.jansi.Ansi.Color.*;

/* java -cp ./bin;./lib/jansi-1.16.jar com.concurrency.Challenge4 */

public class Challenge6 {

	public static void main(String[] args) {
	    AnsiConsole.systemInstall();
		
		BankAccount account = new BankAccount("12345-678", 1000.00);
		account.printBalance(GREEN);

		new Thread (){
			public void run(){
					account.depositWithStatus(300.00, RED);
					account.printBalance(RED);
					account.withdrawWithStatus(50.00, RED);
					account.printBalance(RED);
				
			} 
		}.start();
		
		new Thread (){
			public void run(){
					account.depositWithStatus(203.75, BLUE);
					account.printBalance(BLUE);
					account.withdrawWithStatus(100.00, BLUE);
					account.printBalance(BLUE);
			}
		}.start();
		
		synchronized(account){
			account.printBalance(GREEN);
		}
	}

}
