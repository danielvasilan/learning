package com.concurrency;



import com.concurrency.ThreadColor;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.fusesource.jansi.Ansi.Color;

/**
 * @author daniel.vasilan
 *
 */
public class BankAccount {
	private double balance;
	private String accountNumber;
	private ReentrantLock lock = new ReentrantLock();
	
	public BankAccount(String accountNumber, double balance) {
		super();
		this.balance = balance;
		this.accountNumber = accountNumber;
	}

	/**
	 * This methods are used by the Challenges 1 & 2 of the Java course 
	 * @param amount
	 * @param color
	 */
	public void deposit(double amount, Color color){
		synchronized(this){
			balance += amount;
			ThreadColor.printcolor("Deposit " + amount, color);
		}
	}
	
	public void withdraw (double amount, Color color) {
		synchronized(this){
			balance -= amount;
			ThreadColor.printcolor("Withdraw " + amount, color);
		}
	}

	/**
	 * This methods are used by the Challenge 4 of the Java course 
	 * @param amount
	 * @param color
	 */
	public void depositWithLock (double amount, Color color){
		this.lock.lock();
		ThreadColor.printcolor("Deposit lock", color);
		try{
			balance += amount;
			ThreadColor.printcolor("Deposit " + amount, color);
		} catch (Exception e){
			ThreadColor.printcolor("Deposit exception: " + e.getMessage(), color);
		} finally {
			this.lock.unlock();
			ThreadColor.printcolor("Deposit unlock", color);
		}
	}
	
	public void withdrawWithLock (double amount, Color color) {
		this.lock.lock();
		ThreadColor.printcolor("Withdraw lock", color);
		try{
			balance -= amount;
			ThreadColor.printcolor("Withdraw " + amount, color);
		} catch (Exception e){
			ThreadColor.printcolor("Withdraw exception: " + e.getMessage(), color);
		} finally {
			this.lock.unlock();
			ThreadColor.printcolor("Withdraw unlocked", color);
		}
	}	
	
	/**
	 * This methods are used by the Challenge 5 of the Java course 
	 * @param amount
	 * @param color
	 */
	public void depositWithTimeLock (double amount, Color color){
		try {
			if (lock.tryLock(1000, TimeUnit.MILLISECONDS)){
				ThreadColor.printcolor("Deposit lock", color);
				try{
					balance += amount;
					ThreadColor.printcolor("Deposit " + amount, color);
				} catch (Exception e){
					ThreadColor.printcolor("Deposit exception: " + e.getMessage(), color);
				} finally {
					this.lock.unlock();
					ThreadColor.printcolor("Deposit unlock", color);
				}
			} else {
				ThreadColor.printcolor("Deposit: Could not get the lock", color);
			}
		} catch (InterruptedException e1) {
		}
	}
	
	public void withdrawWithTimeLock (double amount, Color color) {
		try {
			if(lock.tryLock(1000, TimeUnit.MILLISECONDS)){
				ThreadColor.printcolor("Withdraw lock", color);
				try{
					balance -= amount;
					ThreadColor.printcolor("Withdraw " + amount, color);
				} catch (Exception e){
					ThreadColor.printcolor("Withdraw exception: " + e.getMessage(), color);
				} finally {
					this.lock.unlock();
					ThreadColor.printcolor("Withdraw unlocked", color);
				}
			} else {
				ThreadColor.printcolor("Withdraw: Could not get the lock", color);
			}
		} catch (InterruptedException e1) {
		}

	}	
	/**
	 * This methods are used by the Challenge 6 of the Java course 
	 * @param amount
	 * @param color
	 */
	public void depositWithStatus (double amount, Color color){
		boolean status = false;
		try {
			if (lock.tryLock(1000, TimeUnit.MILLISECONDS)){
				ThreadColor.printcolor("Deposit lock", color);
				try{
					balance += amount;
					status = true;
					ThreadColor.printcolor("Deposit " + amount, color);
				} catch (Exception e){
					ThreadColor.printcolor("Deposit exception: " + e.getMessage(), color);
				} finally {
					this.lock.unlock();
					ThreadColor.printcolor("Deposit unlock", color);
				}
			} else {
				ThreadColor.printcolor("Deposit: Could not get the lock", color);
			}
		} catch (InterruptedException e1) {
		}
		ThreadColor.printcolor("Deposit status = " + status, color);
	}
	
	public void withdrawWithStatus (double amount, Color color) {
		boolean status = false;
		try {
			if(lock.tryLock(1000, TimeUnit.MILLISECONDS)){
				ThreadColor.printcolor("Withdraw lock", color);
				try{
					balance -= amount;
					status = true;
					ThreadColor.printcolor("Withdraw " + amount, color);
				} catch (Exception e){
					ThreadColor.printcolor("Withdraw exception: " + e.getMessage(), color);
				} finally {
					this.lock.unlock();
					ThreadColor.printcolor("Withdraw unlocked", color);
				}
			} else {
				ThreadColor.printcolor("Withdraw: Could not get the lock", color);
			}
		} catch (InterruptedException e1) {
		}
		ThreadColor.printcolor("Withdraw status = " + status, color);
	}	
	public void printBalance (Color color){
		ThreadColor.printcolor("Balance is " + this.balance, color);
	}

	public double getBalance() {
		return balance;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
	public void printAccountNumber(){
		System.out.println("Account Number = " + this.accountNumber);
	}
	
	
}
