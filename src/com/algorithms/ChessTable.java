package com.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ChessTable {
	private Integer ROW_NUM = 4;
	private Integer COL_NUM = 4;
	List<List<ChessField>> fields = new LinkedList<List<ChessField>>();
	
	public void init(Integer rownum, Integer colnum){
		this.ROW_NUM = rownum;
		this.COL_NUM = colnum;
		fields.clear();
		for (Integer r=1; r <= ROW_NUM; r++){
			List<ChessField> row = new LinkedList<ChessField>();
			for (Integer c=1; c <= COL_NUM; c++){
				ChessField field = new ChessField("["+r+";"+c+"]");
				row.add(field);
			}
			fields.add(row);
		}
	}
	
	public String toString (){
		StringBuffer text = new StringBuffer();
		text.append("    ");
		for (Integer c=1; c <= COL_NUM; c++){
			text.append(String.format("%3s", c));
		}
		text.append(" |\n");
		text.append("    ");
		for (Integer c=1; c <= COL_NUM; c++){
			text.append("---");
		}
		text.append(" |\n");
		// print rows
		for (Integer r=1; r <= ROW_NUM; r++){
			text.append(String.format("%3s", r) + "|");
			List<ChessField> row = fields.get(r-1);
			for (Integer c=1; c <= COL_NUM; c++){
				text.append(String.format("%3s", row.get(c - 1).getStatus()));
			}
			text.append(" |\n");
		}
		return text.toString();
	}
	
	public void printTable(){
		System.out.println(this.toString());
	}
	
	public void calcNextFields(){
		ArrayList<String> legalMoves = new ArrayList<>(Arrays.asList("-1,-2", "-2,-1", "1,2", "2,1", "-1,2", "-2,1", "1,-2","2,-1"));
		
		for (Integer r=1; r <= ROW_NUM; r++){
			for (Integer c=1; c <= COL_NUM; c++){
				ChessField field = getField(r, c);
				field.setNextFields(calcNextFields(r, c, legalMoves));
			}
		}
		
	}
	
	public ArrayList<ChessField> calcNextFields(Integer r, Integer c, ArrayList<String> legalMoves){
		
		ArrayList<ChessField> nextFields = new ArrayList<ChessField>();
		
		legalMoves.forEach((move) -> {
			Integer new_r = Integer.parseInt(move.split(",")[0]) + r;
			Integer new_c = Integer.parseInt(move.split(",")[1]) + c;
			if(new_r > 0 && new_r <= ROW_NUM && new_c > 0 && new_c <= COL_NUM){
				nextFields.add(this.getField(new_r, new_c));
			};
		});
		
		return nextFields;
	}
	
	public ChessField getField(Integer r, Integer c){
		List<ChessField> row = fields.get(r - 1);
		if (row.size() >= c){
			return row.get(c - 1);
		}
		return null;
	}
	
	public Integer getTableSize (){
		return ROW_NUM * COL_NUM;
	}
	
}