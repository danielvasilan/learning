package com.algorithms;

public class Main {
	static ChessTable table = new ChessTable();
	
	public static void main(String[] args) {
		
		table.init(5, 5);
		table.calcNextFields();
		table.printTable();

		KnightTourSolver solver = new KnightTourSolver(table);
		solver.solve(1, 1);
		solver.printSolutions();
	}
	
}
