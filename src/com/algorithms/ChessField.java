package com.algorithms;

import java.util.List;

public class ChessField {
	private String address;
	private Integer roundNo = -1;
	private List<ChessField> nextFields;
	private Integer lastVisitedFieldIndex = -1;
	
	public ChessField(String address) {
		super();
		this.roundNo = -1;
		this.address = address;
	}
	
	public ChessField() {
		super();
		this.roundNo = -1;
	}
	public String getStatus (){
		return roundNo == -1 ? "_" : roundNo.toString();
	}

	public void setNextFields(List<ChessField> nextFields){
		this.nextFields = nextFields;
	}
	
	public ChessField getNextField(Integer roundNo){
		while (lastVisitedFieldIndex < nextFields.size() - 1){
			lastVisitedFieldIndex ++;
			ChessField nextField = nextFields.get(lastVisitedFieldIndex);
			if (nextField.roundNo == -1){
				nextField.roundNo = roundNo;
				return nextField;
			}
		}
		return null;
	}
	
	public boolean goBack(){
		this.roundNo = -1;
		this.lastVisitedFieldIndex = -1;
		return true;
	}

	public String toString(){
		return address;
	}

	public Integer getRoundNo() {
		return roundNo;
	}

	public void setRoundNo(Integer roundNo) {
		this.roundNo = roundNo;
	}
	
}