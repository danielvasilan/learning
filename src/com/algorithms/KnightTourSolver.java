package com.algorithms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class KnightTourSolver {
	List<String> solutions = new ArrayList<String>();
	ChessField crtField;
	List<ChessField> solutionSteps = new LinkedList<ChessField>();
	
	ChessTable crtTable;
	
	public KnightTourSolver (ChessTable table){
		this.crtTable = table;
	}
	
	public void solve(Integer startRow, Integer startCol){
		crtField = crtTable.getField(startRow, startCol);
		crtField.setRoundNo(1);
		solutionSteps.clear();
		solutionSteps.add(crtField);
		
		while (solutionSteps.size() > 0){

			// is completed
			if (isSolutionComplete()){
				System.out.println("Solution complete. "+ solutions.size());
				solutions.add(crtTable.toString());
				goBack();
			}
			// TEMPORARY add some partial solutions
			if (solutionSteps.size() >= crtTable.getTableSize()){
				System.out.println("Solution almost complete. "+ solutionSteps.size());
				solutions.add("Partial "+solutionSteps.size()+"\n"+crtTable.toString());
			}
			// 
			if (!goNext()){
				//crtTable.printTable();
				goBack();
			}
			//crtTable.printTable();
			
		}
		
	}
	
	public boolean goNext (){		
		ChessField nextField = crtField.getNextField(solutionSteps.size()+1);
		if(nextField != null){
			//sSystem.out.println("Go next " + crtField.toString() + " --> " + nextField.toString() + " " + (solutionSteps.size()+1));
			crtField = nextField;
			solutionSteps.add(crtField);
			return true;
		}
		return false;
	}
	
	private boolean goBack(){
		//System.out.println("Go back " + crtField.toString());
		crtField.goBack();
		solutionSteps.remove(crtField);
		if (solutionSteps.size() == 0){
			System.out.println("No more solutions");
			return false;
		}
		crtField = solutionSteps.get(solutionSteps.size() - 1 );
		return true;
	}
	
	private boolean isSolutionComplete (){
		
		return solutionSteps.size() == crtTable.getTableSize() ? true : false;
	}
	public void printSolutions(){
		System.out.printf("We found %s solutions \n", solutions.size());
		for (String solution : solutions){
			System.out.println(solution);;
		}
	}
	
}
