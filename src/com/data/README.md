## Purpose

The intention with this package is to provide some utility functions for accessing various types of databases/data-sets.

Functions:

    * read data and parse content
    * map to/from Java objects
    * write back to data source
    * DDL commands
    * bulk commands
    * data seed / initial load. 
    * various scripts - DMLs (?)
    * cross technology 
    
[TBD] should the initial data be kept as a json file and all the data-sources to work with it ?


## Target database technologies

1. RDBMS
    * Oracle
    - SQLite
    
2. Graph
    * Neo4j
    
3. Time-series
    * kdb+
    
4. Key-value
    * Infinispan
    * 

5. Document store

6. Other sources 
    * csv
    * xml
    * xls
    * json
    * rest 
    

