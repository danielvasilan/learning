package com.data;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;

public class MySparkUtil {

	public static JavaSparkContext getSession(String serverIp) {
		JavaSparkContext sc = null;
		try {
			SparkConf conf = new SparkConf().setMaster("local").setAppName("Word Count");
			 sc = new JavaSparkContext(conf);
			
			System.out.println("Spark session successfully created");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sc;
	}

	public Dataset<Object> getDataSet() {
		Dataset<Object> ds = null;
		return ds;
	}

}
