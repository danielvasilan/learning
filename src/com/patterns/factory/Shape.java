package com.patterns.factory;

public interface Shape {
	public void draw();
}
