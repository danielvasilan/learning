## PURPOSE

These package will contain implementations for various java patterns.

## Creational

##### Abstract Factory 

[GoF] 
> Provides an interface for creating families of related or dependent objects without specifying their concrete classes.



## Behavioral

##### Visitor 

[GoF] 
> Allows for one or more operation to be applied to a set of objects at runtime, decoupling the operations from the object structure. 

The pattern can provide additional functionality to a class without changing it

> https://dzone.com/articles/design-patterns-visitor
